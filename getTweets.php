<?php
    require_once 'TwitterAPIExchange.php';



    function get_tweets() {
        $settings = array(
            'oauth_access_token' => "3031834246-xie6lxmcidAb8nqyBVTQMYvQx6i0PM2nz1o7Y82",
            'oauth_access_token_secret' => "Y484S2ZP0J5TrOPij3uPApEA38JyRO1RWJpuIjBQ3pe3L",
            'consumer_key' => "XZTsxnkvdjSgIlopeI31kAjEn",
            'consumer_secret' => "Tv3q3SS8p1hDa7h6Z65B8GjYZftyTEyBPKC1sQdd8KSOrWgiWX"
        );
        $url = 'https://api.twitter.com/1.1/search/tweets.json';
        $requestMethod = 'GET';
        $getfields = '?q=from:utsbig&count=7&result_type=mixed';

        $twitter = new TwitterAPIExchange($settings);

        // Get tweets
        $data = $twitter->setGetfield($getfields)
            ->buildOauth($url, $requestMethod)            
            ->performRequest();

        return json_decode($data);   
    }


    function linkify_twitter_status($status_text)
    {
        // linkify URLs
        $status_text = preg_replace(
            '/(https?:\/\/\S+)/',
            '<a target="_BLANK" href="\1">\1</a>',
            $status_text
        );

        // linkify twitter users
        $status_text = preg_replace(
            '/(^|\s)@(\w+)/',
            '\1<a target="_BLANK" href="http://twitter.com/\2">@\2</a>',
            $status_text
        );

        // linkify tags
        $status_text = preg_replace(
            '/(^|\s)#(\w+)/',
            '\1<a target="_BLANK" href="http://twitter.com/search?q=%23\2">#\2</a>',
            $status_text
        );

        return $status_text;
    }


    define("SECOND", 1);
    define("MINUTE", 60 * SECOND);
    define("HOUR", 60 * MINUTE);
    define("DAY", 24 * HOUR);
    define("MONTH", 30 * DAY);
    function relativeTime($time)
    {   
        $delta = time() - strtotime($time);

        if ($delta < 1 * MINUTE)
        {
            return $delta == 1 ? "one second ago" : $delta . " seconds ago";
        }
        if ($delta < 2 * MINUTE)
        {
            return "a minute ago";
        }
        if ($delta < 45 * MINUTE)
        {
            return floor($delta / MINUTE) . " minutes ago";
        }
        if ($delta < 90 * MINUTE)
        {
            return "an hour ago";
        }
        if ($delta < 24 * HOUR)
        {
            return floor($delta / HOUR) . " hours ago";
        }
        if ($delta < 48 * HOUR)
        {
            return "yesterday";
        }
        if ($delta < 30 * DAY)
        {
            return floor($delta / DAY) . " days ago";
        }
        if ($delta < 12 * MONTH)
        {
            $months = floor($delta / DAY / 30);
            return $months <= 1 ? "one month ago" : $months . " months ago";
        }
        else
        {
            $years = floor($delta / DAY / 365);
            return $years <= 1 ? "one year ago" : $years . " years ago";
        }
    }    
    
    function create_JSON() {
        $data = get_tweets();
        $data = $data->statuses;
        
   
        $arr = array();

        foreach ($data as $tweet) {
            array_push($arr, array('id'=>$tweet->id_str,'text'=>linkify_twitter_status($tweet->text),'time'=>relativetime($tweet->created_at)));   
        }
        
        return json_encode($arr);
    }


    echo create_JSON();
        
?>